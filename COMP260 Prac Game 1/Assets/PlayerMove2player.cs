﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove2player : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	public float maxSpeed = 5.0f;

	void Update() {
		if (this.gameObject.tag == "Player 1") {
			
			// get the input values
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal");
			direction.y = Input.GetAxis ("Vertical");

			// scale by the maxSpeed parameter
			Vector2 velocity = direction * maxSpeed;

			// move the object
			transform.Translate (velocity * Time.deltaTime);
		}
		if (this.gameObject.tag == "Player 2") {

			// get the input values
			Vector2 direction;
			direction.x = Input.GetAxis ("Horizontal2");
			direction.y = Input.GetAxis ("Vertical2");

			// scale by the maxSpeed parameter
			Vector2 velocity = direction * maxSpeed;

			// move the object
			transform.Translate (velocity * Time.deltaTime);
		}
	}

}
