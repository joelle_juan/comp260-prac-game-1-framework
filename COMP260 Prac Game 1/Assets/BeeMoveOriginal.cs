﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMoveOriginal : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    public float speed = 4.0f;        // metres per second
    public float turnSpeed = 180.0f;  // degrees per second
    public Transform target;
    private Vector2 heading = Vector2.right;

    void Update()
    {
        // get the vector from the bee to the target 
        Vector2 direction = target.position - transform.position;

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);
    }

}
